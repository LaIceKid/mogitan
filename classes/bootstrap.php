<?php
class Bootstrap{
	private $controller;
	private $action;
	private $request;

	public function __construct($request){
		////////////////////////////////////
		// DEFAULTS ////////////////////////

		//Default language on load
		if(!isset($_SESSION['lang'])){
			$_SESSION['lang'] = 'ge';
		}
		if(!isset($_SESSION['currency'])){
			$_SESSION['currency'] = 'GEL';
		}

		// DEFAULTS ////////////////////////
		////////////////////////////////////

		$routes = new Route;
		$route = $routes->router();

		$this->request = $request;
		if(isset($this->request['route']) && isset($route[$this->request['route']])){
			$this->controller = $route[$this->request['route']]['controller'];
			$this->action = $route[$this->request['route']]['action'];
		}
	}

	public function createController(){
		//check class
		if(class_exists($this->controller)){
			$parents = class_parents($this->controller);
			//check extend
			if(in_array("Controller", $parents)){
				if(method_exists($this->controller, $this->action)){
					return new $this->controller($this->action, $this->request);
				}else{
					//method doesnt exist
					// echo '<h1>Method Does not exist</h1>';
					header("HTTP/1.0 404 Not Found");
					include($_SERVER['DOCUMENT_ROOT'].'/404.shtml');
					return;
				}
			}else{
				//base controller doesnt exists
				// echo '<h1>Base controller not found</h1>';
				header("HTTP/1.0 404 Not Found");
				include($_SERVER['DOCUMENT_ROOT'].'/404.shtml');
				return;
			}
		}else{
			//Controller class doesnt exists
			// echo '<h1>Controller class desnt exists</h1>';
			header("HTTP/1.0 404 Not Found");
			include($_SERVER['DOCUMENT_ROOT'].'/404.shtml');
			return;
		}
	}
}
