<?php

class Route extends Model {
	public function router() {
		$route = array(
			''              => array(
				'controller' => 'home',
				'action'     => 'index'
			),
			'timeline'      => array(
				'controller' => 'home',
				'action'     => 'timeline'
			),
			'login'         => array(
				'controller' => 'user',
				'action'     => 'login'
			),
			'register'      => array(
				'controller' => 'user',
				'action'     => 'register'
			),
			'items'       => array(
				'controller' => 'user',
				'action'     => 'items'
			),
			'checklogin'    => array(
				'controller' => 'user',
				'action'     => 'checklogin'
			),
			'checkregister' => array(
				'controller' => 'user',
				'action'     => 'checkregister'
			),
			'logout'        => array(
				'controller' => 'user',
				'action'     => 'logout'
			),
			'language'      => array(
				'controller' => 'language',
				'action'     => 'select'
			),
			'add'      => array(
				'controller' => 'user',
				'action'     => 'add'
			),
		);
		$route = array_merge($route, $this->category_router());

		// echo '<pre class="test-div" style="position:fixed;left:0;top:0;height:100%;">';
		// print_r($route);
		// echo '</pre>';

		return $route;
	}

	public function category_router() {
		$HomeModel = new HomeModel;
		$this->query("SELECT * FROM category WHERE parentID = 0");
		$categories = $this->resultSet();

		$build = array();
		foreach ($categories as $category) {
			$slug = strtolower(str_replace('_', '-', $category['category_name']));
			$build[$slug] = array(
				'controller' => 'category',
				'action'     => 'select'
			);

			foreach ($HomeModel->category($category['id']) as $subCat) {
				$subSlug = strtolower(str_replace('_', '-', $subCat['category_name']));
				$build[$subSlug] = array(
					'controller' => 'category',
					'action'     => 'select',
					'parent'     => $slug
				);
			}
		}
		return $build;
	}
}
