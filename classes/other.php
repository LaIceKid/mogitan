<?php

class Other {
	public function account_id($id = 0) {
		return str_pad($id, 5, 0, STR_PAD_LEFT);
	}

	public function account_id_reverse($account_id = 0) {
		$trim = ltrim($account_id, '00000');
		return $trim;
	}

	public function breadcrumbs() {
		$Route = new Route();
		$LanguageModel = new LanguageModel;
		$lang = $LanguageModel->SelectLanguage();

		$crumb = array(0 => $lang['HOME']);
		foreach ($Route->router() as $key => $router) {
			if ($key = $_GET['route']) {
				if (isset($lang[strtoupper(str_replace('-', '_', $key))])) {
					$crumb[] = '<span class="crumb text-gray">' . $lang[strtoupper(str_replace('-', '_', $key))] . '</span>';
					break;
				}
			}
		}
		foreach ($Route->router() as $key => $router) {
			if ($key = $_GET['subroute']) {
				if (isset($lang[strtoupper(str_replace('-', '_', $key))])) {
					$crumb[] = '<span class="crumb text-gray">' . $lang[strtoupper(str_replace('-', '_', $key))] . '</span>';
					break;
				}
			}
		}
		return implode('<span class="crumb-separator"><span class="oi oi-chevron-right"></span></span>', $crumb);
	}

	public function image_upload($image = array(), $directory = '') {
		if (empty($image['name'])) {
			return NULL;
		}
		$file = $image['name'];
		$tmp = $image['tmp_name'];
		$filesize = $image['size'];
		$size = floor($filesize / 1024);
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		$ext = strtolower($ext);

		if ($ext != "jpg" && $ext != "jpeg" && $ext != "png" && $ext != "gif") {
			return NULL;
		}

		if ($ext == "jpg" || $ext == "jpeg") {
			$src = imagecreatefromjpeg($tmp);
		} elseif ($ext == "png") {
			$src = imagecreatefrompng($tmp);
			imagealphablending($src, false);
			imagesavealpha($src, true);
		} else {
			$src = imagecreatefromgif($tmp);
		}

		list($width, $height) = getimagesize($tmp);

		if ($width > $height) {
			$new_width = 400;
			$new_height = ($height / $width) * $new_width;
		} else {
			$new_height = 400;
			$new_width = ($width / $height) * $new_height;
		}

		$resize = imagecreatetruecolor($new_width, $new_height);
		imagecopyresampled($resize, $src, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		$dir = $directory;
		$filename = MD5(time() . $image["name"]) . '.' . $ext;
		$full = $dir . $filename;

		if ($ext == 'png') {
			imagepng($src, $full, 9);
		} else {
			imagejpeg($resize, $full, 80);
		}

		return $filename;

//		echo '<strong>Orig File:</strong> ' . $file . '<br />';
//		echo '<strong>Tmp Name:</strong> ' . $tmp . '<br />';
//		echo '<strong>Extension:</strong> ' . $ext . '<br />';
//		echo '<strong>Width x Height:</strong> ' . $width . ' X ' . $height . '<br />';
//		echo '<strong>New Width x Height:</strong> ' . $new_width . ' X ' . $new_height . '<br />';
//		echo '<strong>Size:</strong> ' . $size . '<br />';
//		echo '<strong>New File Name:</strong> ' . $filename . '<br />';
//		echo '<br /><img src="' . $directory . $filename . '" /><br /><br />';
	}

	public function access() {
		if (!isset($_POST['key'])) {
			Messages::setMsg("YOU_DONT_HAVE_A_PERMISSION_TO_SEE_THIS_PAGE", "error");
			header("location:" . ROOT_URL);
		}
	}
}
