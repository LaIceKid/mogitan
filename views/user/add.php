<div class="row mt-4">

	<div class="col-12 col-md-6 py-3">
		<form action="">
			<div class="form-group">
				<label for="item-name">ნივთის დასახელება</label>
				<input id="item-name" type="text" class="form-control" placeholder="შეიყვანეთ ნივთის დასახელება">

				<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
			</div>
		</form>
	</div>

	<div class="col-12 col-md-6 bg-light border rounded-sm py-3">
		<label for="item-name">გაიცვლება</label>
		<input id="item-name" type="text" class="form-control mb-3" placeholder="">

		<span class="m-1 float-left badge badge-primary">მაუსი</span>
		<span class="m-1 float-left badge badge-secondary">i7</span>
		<span class="m-1 float-left badge badge-success">i5</span>
		<span class="m-1 float-left badge badge-danger">Nvidia GeForce 990</span>
		<span class="m-1 float-left badge badge-warning">სხვა</span>
		<span class="m-1 float-left badge badge-info">კლავიატურა</span>
		<span class="m-1 float-left badge badge-light">მონიტორი</span>
		<span class="m-1 float-left badge badge-dark">სმარტფონი</span>
		<span class="m-1 float-left badge badge-primary">i9</span>
		<span class="m-1 float-left badge badge-secondary">საათი</span>
		<span class="m-1 float-left badge badge-success">პლანშეტი</span>
		<span class="m-1 float-left badge badge-danger">Sony Swr3</span>
		<span class="m-1 float-left badge badge-primary">Primary</span>
		<span class="m-1 float-left badge badge-secondary">Secondary</span>
		<span class="m-1 float-left badge badge-success">Success</span>
		<span class="m-1 float-left badge badge-danger">Danger</span>
		<span class="m-1 float-left badge badge-warning">Warning</span>
		<span class="m-1 float-left badge badge-info">Info</span>
		<span class="m-1 float-left badge badge-light">Light</span>
		<span class="m-1 float-left badge badge-dark">Dark</span>
		<span class="m-1 float-left badge badge-primary">Primary</span>
		<span class="m-1 float-left badge badge-secondary">Secondary</span>
		<span class="m-1 float-left badge badge-success">Success</span>
		<span class="m-1 float-left badge badge-danger">Danger</span>
		<span class="m-1 float-left badge badge-primary">Primary</span>
		<span class="m-1 float-left badge badge-secondary">Secondary</span>
		<span class="m-1 float-left badge badge-success">Success</span>
		<span class="m-1 float-left badge badge-danger">Danger</span>
		<span class="m-1 float-left badge badge-warning">Warning</span>
		<span class="m-1 float-left badge badge-info">Info</span>
		<span class="m-1 float-left badge badge-light">Light</span>
		<span class="m-1 float-left badge badge-dark">Dark</span>
		<span class="m-1 float-left badge badge-primary">წიგნი</span>
		<span class="m-1 float-left badge badge-secondary">Secondary</span>
		<span class="m-1 float-left badge badge-success">ნათურა</span>
		<span class="m-1 float-left badge badge-danger">Danger</span>
		<span class="m-1 float-left badge badge-primary">Primary</span>
		<span class="m-1 float-left badge badge-secondary">Secondary</span>
		<span class="m-1 float-left badge badge-success">Success</span>
		<span class="m-1 float-left badge badge-danger">Danger</span>
		<span class="m-1 float-left badge badge-warning">Warning</span>
		<span class="m-1 float-left badge badge-info">Info</span>
		<span class="m-1 float-left badge badge-light">Light</span>
		<span class="m-1 float-left badge badge-dark">Dark</span>
		<span class="m-1 float-left badge badge-primary">Primary</span>
		<span class="m-1 float-left badge badge-secondary">Secondary</span>
		<span class="m-1 float-left badge badge-success">Success</span>
		<span class="m-1 float-left badge badge-danger">Danger</span>
		<span class="m-1 float-left badge badge-primary">Primary</span>
		<span class="m-1 float-left badge badge-secondary">Secondary</span>
		<span class="m-1 float-left badge badge-success">Success</span>
		<span class="m-1 float-left badge badge-danger">Danger</span>
		<span class="m-1 float-left badge badge-warning">Warning</span>
		<span class="m-1 float-left badge badge-info">Info</span>
		<span class="m-1 float-left badge badge-light">Light</span>
		<span class="m-1 float-left badge badge-dark">Dark</span>
		<span class="m-1 float-left badge badge-primary">Primary</span>
		<span class="m-1 float-left badge badge-secondary">ლეპტოპი</span>
		<span class="m-1 float-left badge badge-success">კედლის საათი</span>
		<span class="m-1 float-left badge badge-danger">ველოსიპედი</span>

	</div>

</div>