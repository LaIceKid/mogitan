<div class="user-register-container" style="border-radius:5px;">
    <div class="user-register-header">
      <h4>რეგისტრაცია</h4>
      <div class="user-register-close">
        <a href="#" class="text-gray" data-dismiss="modal">დახურვა <span class="oi oi-x"></span></a>
      </div>
    </div>

    <form method="post" action="<?php echo ROOT_URL; ?>checkregister/" id="register-form" enctype='multipart/form-data'>
    <div class="user-register-body">

      <label for="first-name" class="first-name-label">სახელი, გვარი</label>
        <div class="input-icon left-addon">

        <div class="input-group mb-3">
            <input type="first-name" name="first-name" class="form-control" id="first-name" required>
            <input type="last-name" name="last-name" class="form-control" id="last-name" required>
        </div>
        <span class="oi oi-person"></span>
      </div>


      <label for="email" class="email-label">ელ. ფოსტა:</label>
      <div class="input-icon left-addon">
        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" required>
        <span class="oi oi-envelope-closed"></span>
      </div>
      <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->

      <label for="mobile" class="mobile-label">მობილურის ნომერი:</label>
      <div class="input-icon left-addon">
        <input type="tel" name="mobile" class="form-control" id="mobile" required>
        <span class="oi oi-phone"></span>
      </div>

      <label for="password" class="password-label">პაროლი:</label>
      <div class="input-icon left-addon">
        <input type="password" name="password" class="form-control" id="password" required>
        <span class="oi oi-lock-locked"></span>
      </div>

      <label for="re-password" class="re-password-label">გაიმეორეთ პაროლი:</label>
      <div class="input-icon left-addon">
        <input type="password" name="re-password" class="form-control" id="re-password" required>
        <span class="oi oi-lock-locked"></span>
      </div>

      <div class="input-icon left-addon">
        <label for="avatar" class="avatar-label">ავატარის დამატება >>
        <label class="btn btn-file register-custom-upload-btn"><i class="fa fa-file-o"></i>
        არჩევა <input type="file" name="file" style="display: none;" id="files"></label>
        </label>

        <?php //we can make upload multiple images by attribute multiple but here it is not necessary - multiple="" ?>

        <div>
					<ul id="selectedFiles" style="padding-left:15px;margin-top:10px;"></ul>
				</div>
        <script>
					$("#files").on('change', function(){
						var names = [];
						for (var i = 0; i < $(this).get(0).files.length; ++i) {
							names.push('<li>' + $(this).get(0).files[i].name + '</li>');
						}
						$("#selectedFiles").html(names);
					});
				</script>
      </div>


    </div>

    <div class="row user-register-footer">
      <div class="col-sm-5 order-sm-2">
        <button type="submit" class="btn btn-custom-green float-sm-right" href="#">რეგისტრაცია</button>
      </div>
      <div class="col-sm-7 order-sm-1 user-register-alt-login">
        <div class="text-green">ავტორიზაცია</div>
        გაიარე რეგისტრაცია Facebook-ის საშუალებით
      </div>
    </div>
  </form>
</div>
