<div class="user-login-container" style="border-radius:5px;">
    <div class="user-login-header">
      <h4>ავტორიზაცია</h4>
      <div class="user-login-close">
        <a href="#" class="text-gray" data-dismiss="modal">დახურვა <span class="oi oi-x"></span></a>
      </div>
    </div>

    <form method="post" action="<?php echo ROOT_URL; ?>checklogin/" id="login-form">
    <div class="user-login-body">

        <label for="email" class="email-label">ელ. ფოსტა:</label>
      <div class="input-icon left-addon">
        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp">
        <span class="oi oi-envelope-closed"></span>
      </div>
      <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        <label for="password" class="password-label">პაროლი:</label>
      <div class="input-icon left-addon">
        <input type="password" name="password" class="form-control" id="password">
        <span class="oi oi-lock-locked"></span>
      </div>
    </div>

    <div class="row user-login-footer">
      <div class="col-sm order-sm-3">
        <button type="submit" class="btn btn-custom-orange float-sm-right" href="#">შესვლა</button>
      </div>
      <div class="col-sm order-sm-2">
        <a href="#" class="text-orange">რეგისტრაცია</a>
      </div>
      <div class="col-sm order-sm-1">
        <a href="#" class="text-gray">პაროლის აღდგენა</a>
      </div>
    </div>
  </form>
</div>
