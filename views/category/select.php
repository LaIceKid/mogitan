<?php if(empty($viewmodel)){ ?>
  <div class="alert alert-warning category-warning">
    <?php echo $lang['EMPTY_CATEGORY']; ?>
  </div>
<?php } ?>
<div class="row products">
  <?php foreach ($viewmodel as $key => $item){ ?>
    <div class="col-lg-4">
      <div class="product">
        <div class="product-image" style="
          background: url('<?php echo ROOT_URL; ?>assets/images/product_images/<?php echo $item['images'][0]; ?>') no-repeat center center;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        ">
          <?php if(!empty($item['discounts'])){ ?>
            <div class="discount">
              <span class="badge badge-<?php echo $HomeModel->discount_color(array_sum($item['discounts'])); ?>">-<?php echo array_sum($item['discounts']); ?>%</span>
            </div>
          <?php } ?>
        </div>
        <div class="product-name">
          <?php echo $item['product_name']; ?>
        </div>
        <div class="product-price">
          <?php if(!empty($item['discounts'])){ ?>
            <span class="old-price">
              <?php echo $CurrencyModel->convert($item['product_price'], $_SESSION['currency']); ?>
              <?php echo $CurrencyModel->dispCurrency($_SESSION['currency']); ?>
           </span>
          <span class="new-price discounted-price">
            <?php echo $CurrencyModel->convert($item['discounted_price'], $_SESSION['currency']); ?>
            <?php echo $CurrencyModel->dispCurrency($_SESSION['currency']); ?>
          </span>
          <?php }else{ ?>
            <span class="new-price">
              <?php echo $CurrencyModel->convert($item['product_price'], $_SESSION['currency']); ?>
              <?php echo $CurrencyModel->dispCurrency($_SESSION['currency']); ?>
            </span>
          <?php } ?>
          <span class="badge badge-info" data-toggle="popover" title="როგორ მუშაობს კონვერტაცია სხვა ვალუტაში" data-content="ჩვენ ყოველდღიურად ვიღებთ საქართველოს ეროვნული ბანკიდან ვალუტის კურსებს. დღევანდელი ფასი არა ეროვნულ ვალუტაში შეიძლება იყოს ცვალებადი.">?</span>
        </div>
        <div class="product-status row">
          <div class="col-7 <?php echo $item['status_color']; ?>">
            <?php echo $lang[$item['product_status']]; ?>
          </div>
          <div class="col-5">
            <a class="btn btn-custom-green" href="#">სრულად</a>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
</div>
