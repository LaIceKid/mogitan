<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <?php $slideshow = $HomeModel->slider(); ?>
  <ol class="carousel-indicators">
    <?php foreach($slideshow as $sliderIndicator){ ?>
      <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $sliderIndicator['sort']; ?>" class="<?php echo $sliderIndicator['sort'] == 1?'active':''; ?>"></li>
    <?php } ?>
  </ol>
  <div class="carousel-inner">
    <?php foreach($slideshow as $slider){ ?>
      <div class="carousel-item <?php echo $slider['sort'] == 1?'active':''; ?>" style="
        background: url('<?php echo ROOT_URL; ?>assets/images/slider_images/<?php echo $slider['slider_image']; ?>') no-repeat center center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
      ">
        <div class="carousel-caption d-none d-md-block">
          <h5><?php echo $slider['slider_title']; ?></h5>
          <p><?php echo $slider['slider_description']; ?></p>
        </div>
      </div>
    <?php } ?>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div class="filter-menu">
  <nav class="navbar navbar-expand-lg navbar-custom navbar-filter-custom">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#"><div class="active-hover-link"></div>ყველა პროდუქტი</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><div class="active-hover-link"></div>ყველაზე გაყიდვადი</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><div class="active-hover-link"></div>დღის შეთავაზებები</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><div class="active-hover-link"></div>კოლექციები</a>
      </li>
    </ul>
  </nav>
</div>


<div class="row filters">
  <div class="col-lg-8 search-bar input-icon left-addon-extra">
    <span class="oi oi-magnifying-glass"></span>
    <input class="form-control form-control-lg" type="text" placeholder="პროდუქტის ძებნა">
    <a class="btn btn-custom-orange" href="#">ძებნა</a>
  </div>
  <div class="col-lg-4 sorter">
    <select class="form-control form-control-lg">
      <option>თარიღის მიხედვით</option>
    </select>

  </div>
</div>


<div class="row products ajax-loader">
  <div class="col-lg-8">
    <p class="animate-bg">
      <br><br><br><br><br><br><br><br><br><br>
    </p>
  </div>
  <div class="col-sm-6">
    <p class="animate-bg col-sm-11"></p>
    <p class="animate-bg col-sm-9"></p>
    <p class="animate-bg col-sm-10"></p>
    <p class="animate-bg col-sm-9"></p>
    <p class="animate-bg"></p>
  </div>
</div>

<div id="timeline">

</div>
