<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <?php $slideshow = $HomeModel->slider(); ?>
  <ol class="carousel-indicators">
    <?php foreach($slideshow as $sliderIndicator){ ?>
      <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $sliderIndicator['sort']; ?>" class="<?php echo $sliderIndicator['sort'] == 1?'active':''; ?>"></li>
    <?php } ?>
  </ol>
  <div class="carousel-inner">
    <?php foreach($slideshow as $slider){ ?>
      <div class="carousel-item <?php echo $slider['sort'] == 1?'active':''; ?>" style="
        background: url('<?php echo ROOT_URL; ?>assets/images/slider_images/<?php echo $slider['slider_image']; ?>') no-repeat center center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
      ">
        <div class="carousel-caption d-none d-md-block">
          <h5><?php echo $slider['slider_title']; ?></h5>
          <p><?php echo $slider['slider_description']; ?></p>
        </div>
      </div>
    <?php } ?>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div class="filter-menu">
  <nav class="navbar navbar-expand-lg navbar-custom navbar-filter-custom">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#"><div class="active-hover-link"></div>ყველა პროდუქტი</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><div class="active-hover-link"></div>ყველაზე გაყიდვადი</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><div class="active-hover-link"></div>დღის შეთავაზებები</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><div class="active-hover-link"></div>კოლექციები</a>
      </li>
    </ul>
  </nav>
</div>


<div class="row filters">
  <div class="col-lg-8 search-bar input-icon left-addon-extra">
    <span class="oi oi-magnifying-glass"></span>
    <input class="form-control form-control-lg" type="text" placeholder="პროდუქტის ძებნა">
    <a class="btn btn-custom-orange" href="#">ძებნა</a>
  </div>
  <div class="col-lg-4 sorter">
    <select class="form-control form-control-lg">
      <option>თარიღის მიხედვით</option>
    </select>

  </div>
</div>

<div class="row products">
  <?php $products = $HomeModel->all_products(); ?>
  <!-- <pre>
  <?php print_r($products); ?>
  </pre> -->
  <?php foreach ($products as $key => $item){ ?>
    <div class="col-lg-4">
      <div class="product">
        <div class="product-image" style="
          background: url('<?php echo ROOT_URL; ?>assets/images/product_images/<?php echo $item['images'][0]; ?>') no-repeat center center;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        ">
          <?php if(!empty($item['discounts'])){ ?>
            <div class="discount">
              <span class="badge badge-<?php echo $HomeModel->discount_color(array_sum($item['discounts'])); ?>">-<?php echo array_sum($item['discounts']); ?>%</span>
            </div>
          <?php } ?>
        </div>
        <div class="product-name">
          <?php echo $item['product_name']; ?>
        </div>
        <div class="product-price">
          <?php if(!empty($item['discounts'])){ ?>
            <span class="old-price">
              <?php echo $CurrencyModel->convert($item['product_price'], $_SESSION['currency']); ?>
              <?php echo $CurrencyModel->dispCurrency($_SESSION['currency']); ?>
           </span>
          <span class="new-price discounted-price">
            <?php echo $CurrencyModel->convert($item['discounted_price'], $_SESSION['currency']); ?>
            <?php echo $CurrencyModel->dispCurrency($_SESSION['currency']); ?>
          </span>
          <?php }else{ ?>
            <span class="new-price">
              <?php echo $CurrencyModel->convert($item['product_price'], $_SESSION['currency']); ?>
              <?php echo $CurrencyModel->dispCurrency($_SESSION['currency']); ?>
            </span>
          <?php } ?>
          <span class="badge badge-info" data-toggle="popover" title="როგორ მუშაობს კონვერტაცია სხვა ვალუტაში" data-content="ჩვენ ყოველდღიურად ვიღებთ საქართველოს ეროვნული ბანკიდან ვალუტის კურსებს. დღევანდელი ფასი არა ეროვნულ ვალუტაში შეიძლება იყოს ცვალებადი.">?</span>
        </div>
        <div class="product-status row">
          <div class="col-7 <?php echo $item['status_color']; ?>">
            <?php echo $lang[$item['product_status']]; ?>
          </div>
          <div class="col-5">
            <a class="btn btn-custom-green" href="#">სრულად</a>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
</div>
