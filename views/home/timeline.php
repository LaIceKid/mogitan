<?php $HomeModel = new HomeModel; ?>
<div class="row products timeline">
  <?php foreach ($viewmodel as $key => $item){ ?>
    <div class="col-lg-8">
      <div class="product" data-id="<?php echo $item['pid']; ?>" data-last-item-id="<?php echo $HomeModel->last_item(); ?>">
        <div class="product-image" style="
          background: url('<?php echo ROOT_URL; ?>assets/images/product_images/<?php echo $item['images'][0]; ?>') no-repeat center center;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        ">
          <?php if(!empty($item['discounts'])){ ?>
            <div class="discount">
              <span class="badge badge-<?php echo $item['discount_color']; ?>">-<?php echo array_sum($item['discounts']); ?>%</span>
            </div>
          <?php } ?>
          <!-- <div class="timeline-product-images">
            <?php foreach ($item['images'] as $img_key => $ime_val){ ?>
              <img src="<?php echo ROOT_URL; ?>assets/images/product_images/<?php echo $ime_val; ?>" alt="">
            <?php } ?>
          </div> -->
        </div>
        <div class="product-name">
          <?php echo $item['product_name']; ?>
        </div>
        <div class="product-price">
          <?php if(!empty($item['discounts'])){ ?>
            <span class="old-price">
              <?php echo $CurrencyModel->convert($item['product_price'], $_SESSION['currency']); ?>
              <?php echo $CurrencyModel->dispCurrency($_SESSION['currency']); ?>
           </span>
          <span class="new-price discounted-price">
            <?php echo $CurrencyModel->convert($item['discounted_price'], $_SESSION['currency']); ?>
            <?php echo $CurrencyModel->dispCurrency($_SESSION['currency']); ?>
          </span>
          <?php }else{ ?>
            <span class="new-price">
              <?php echo $CurrencyModel->convert($item['product_price'], $_SESSION['currency']); ?>
              <?php echo $CurrencyModel->dispCurrency($_SESSION['currency']); ?>
            </span>
          <?php } ?>
          <span class="badge badge-info" data-toggle="popover" title="როგორ მუშაობს კონვერტაცია სხვა ვალუტაში" data-content="ჩვენ ყოველდღიურად ვიღებთ საქართველოს ეროვნული ბანკიდან ვალუტის კურსებს. დღევანდელი ფასი არა ეროვნულ ვალუტაში შეიძლება იყოს ცვალებადი.">?</span>
        </div>
        <div class="product-status row">
          <div class="col-7 <?php echo $item['status_color']; ?>">
            <?php echo $lang[$item['product_status']]; ?>
          </div>
          <div class="col-5">
            <a class="btn btn-custom-green" href="#">სრულად</a>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
</div>
