<?php
//Get Language
$LanguageModel = new LanguageModel;
$lang = $LanguageModel->SelectLanguage();
//Get Currency
$CurrencyModel = new CurrencyModel;
//Get Category Menu
$HomeModel = new HomeModel;
$Other = new Other;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>
		MOGITAN.GE</title>
	<meta charset="utf-8">
	<meta content="IE=edge"
	      http-equiv="X-UA-Compatible"
	>
	<meta content="Green Man"
	      name="description"
	>
	<meta content="width=device-width, initial-scale=1"
	      name="viewport"
	>
	<link rel="stylesheet"
	      href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	      crossorigin="anonymous"
	>
	<link rel="stylesheet"
	      href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.min.css"
	/>
	<link href="<?php echo ROOT_URL; ?>assets/css/style.css"
	      rel="stylesheet"
	      type="text/css"
	>
	<link rel="apple-touch-icon"
	      sizes="76x76"
	      href="<?php echo ROOT_PATH; ?>assets/images/favicon/apple-touch-icon.png"
	>
	<link rel="icon"
	      type="image/png"
	      sizes="32x32"
	      href="<?php echo ROOT_PATH; ?>assets/images/favicon/favicon-32x32.png"
	>
	<link rel="icon"
	      type="image/png"
	      sizes="16x16"
	      href="<?php echo ROOT_PATH; ?>assets/images/favicon/favicon-16x16.png"
	>
	<link rel="manifest"
	      href="<?php echo ROOT_PATH; ?>assets/images/favicon/site.webmanifest"
	>
	<meta name="msapplication-TileColor"
	      content="#da532c"
	>
	<meta name="theme-color"
	      content="#ffffff"
	>
</head>
<body>
<div class="test-div d-none"
     style="display:block;"
>
  	<pre class="test-pre">
      <?php print_r($_SESSION); ?>
		</pre>
</div>

<div class="outer-navbar">
	<nav class="navbar navbar-expand-lg navbar-custom navbar-dark">
		<a class="navbar-brand col-3" href="<?php echo ROOT_URL; ?>">
			<span class="text-green">M</span>ogitan.ge
		</a>
		<button class="navbar-toggler"
		        type="button"
		        data-toggle="collapse"
		        data-target="#navbarNav"
		        aria-controls="navbarNav"
		        aria-expanded="false"
		        aria-label="Toggle navigation"
		>
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse"
		     id="navbarNav"
		>
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link"
					   href="<?php echo ROOT_URL; ?>"
					>
						<div class="active-hover-link"></div><?php echo $lang['HOME']; ?>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link"
					   href="#"
					>
						<div class="active-hover-link"></div>
						ჩვენს შესახებ
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link"
					   href="#"
					>
						<div class="active-hover-link"></div>
						კონტაქტი
					</a>
				</li>
			</ul>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link"
					   href="#"
					>
						<div class="active-hover-link"></div>
						ფასდაკლებები
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link"
					   href="#"
					>
						<div class="active-hover-link"></div>
						სიახლეები
					</a>
				</li>
			</ul>
		</div>
	</nav>
</div>

<main>
	<div class="row">
		<div class="col-md col-md-3">
			<div class="d-none d-md-block">
				<?php if (isset($_SESSION['user_data']['is_logged_in']) && $_SESSION['user_data']['is_logged_in'] == 1) { ?>
					<div class="login-logged-in">
						<div class="row login-logged-in-header">
							<div class="col-sm col-md-3 order-sm-1">
								<div class="avatar"
								     style="
										     background: url('<?php echo ROOT_URL; ?>assets/images/avatar/<?php echo !empty($_SESSION['user_data']['avatar']) ? $_SESSION['user_data']['avatar'] : 'no-avata.svg'; ?>') no-repeat center center;
										     -webkit-background-size: cover;
										     -moz-background-size: cover;
										     -o-background-size: cover;
										     background-size: cover;
										     "
								>
								</div>
							</div>
							<div class="col-sm col-md-9 order-sm-2">
								<div class="login-name-surname">
									<?php echo $_SESSION['user_data']['first-name']; ?>
									<?php echo $_SESSION['user_data']['last-name']; ?>
									<br>
								</div>
								ანგარიშის ნომერი: <?php echo $Other->account_id($_SESSION['user_data']['id']); ?>
								<br>
								<a href="<?php echo ROOT_URL; ?>items/" class="text-light">
									ჩემი ნივთები >>>
								</a>
							</div>
						</div>
						<div class="login-logged-in-body">
							<div class="login-logged-in-balance">
								ბალანსი: 245 ₾
							</div>
							<div>
								<a href="#"
								   class="text-green"
								>ბალანსის შევსება >>>
								</a>
							</div>
							<div class="login-logged-in-messages">
								შეტყობინებები
								<span class="badge badge-pill background-orange">22</span>
							</div>
							<div class="login-logged-in-favourites">
								რჩეულები: 12
							</div>
						</div>
						<div class="row login-logged-in-footer">
							<div class="col-sm"
							     style="text-align:left;"
							>
								<div class="gold-coin-sign"></div>
								ქულა: 450
							</div>
							<div class="col-sm"
							     style="text-align:right;"
							>
								<a class="btn btn-light"
								   href="<?php echo ROOT_URL; ?>logout/"
								>გასვლა
								</a>
							</div>
						</div>
					</div>
				<?php } else { ?>
					<div class="row login-register-btns">
						<div class="col-5">
							<a class="btn btn-custom-gray"
							   data-toggle="modal"
							   data-target="#login-modal"
							   onclick="ajaxProcess($('#login-container'),'login-container');"
							   href="#"
							>შესვლა
							</a>
						</div>
						<div class="col-7">
							<a class="btn btn-custom-green"
							   data-toggle="modal"
							   data-target="#register-modal"
							   onclick="ajaxProcess($('#register-container'),'register-container');"
							   href="#"
							>რეგისტრაცია
							</a>
						</div>
					</div>
				<?php } ?>
				<hr>
				<div class="cart">
					<img src="<?php echo ROOT_URL; ?>assets/images/icons/cart.png" />
					რაოდენობა:
					<span class="values"><span
								style="color:#f15018"
						>5</span> / 578 ლარი</span>
				</div>
				<div class="favourites">
					<img src="<?php echo ROOT_URL; ?>assets/images/icons/favourite.png" />
					რჩეულები:
					<span class="values">30</span>
				</div>


				<div class="categores">
					<?php foreach ($HomeModel->category() as $cat) { ?>
						<div class="category">
							<a href="<?php echo ROOT_PATH . strtolower(str_replace('_', '-', $cat['category_name'])) . '/'; ?>">
								<img src="<?php echo ROOT_URL; ?>assets/images/icons/<?php echo $cat['icon']; ?>"
								     alt="User Avatar"
								/>
								<?php echo $lang[$cat['category_name']]; ?>
							</a>
							<hr>
							<?php foreach ($HomeModel->category($cat['id']) as $subCat) { ?>
								<div class="sub-category">
									<a href="<?php echo ROOT_PATH . strtolower(str_replace('_', '-', $cat['category_name'])) . '/' . strtolower(str_replace('_', '-', $subCat['category_name'])); ?>/"><?php echo $lang[$subCat['category_name']]; ?></a>
								</div>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="content col-md col-md-9">
			<div class="breadcrumbs">
				<?php echo $Other->breadcrumbs(); ?>
			</div>
			<?php Messages::display(); ?>
			<?php require($view); ?>
		</div>
	</div>
</main>

<div class="modal fade"
     id="login-modal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="login-container-Title"
     aria-hidden="true"
>
	<div class="modal-dialog modal-dialog-centered"
	     role="document"
	>
		<div class="modal-content">
			<div id="login-container"
			     data-href="<?php echo ROOT_URL; ?>login/"
			></div>
		</div>
	</div>
</div>

<div class="modal fade"
     id="register-modal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="register-container-Title"
     aria-hidden="true"
>
	<div class="modal-dialog modal-dialog-centered"
	     role="document"
	>
		<div class="modal-content">
			<div id="register-container"
			     data-href="<?php echo ROOT_URL; ?>register/"
			></div>
		</div>
	</div>
</div>


<script
		src="https://code.jquery.com/jquery-3.4.1.min.js"
		integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
		crossorigin="anonymous"
></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"
></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"
></script>
<script charset="utf-8"
        src="<?php echo ROOT_URL; ?>assets/js/main.js"
        type="text/javascript"
></script>
</body>
</html>
