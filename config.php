<?php
  date_default_timezone_set('Asia/Tbilisi');

  //Define db params
  define("DB_HOST", "localhost");
  define("DB_USER", "root");
  define("DB_PASS", "");
  define("DB_NAME", "mogitan");

  //Define URL(s)
  define("ROOT_PATH", "/");
  define("ROOT_URL", "http://mogitan.loc/");
