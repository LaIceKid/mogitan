<?php
class Language extends Controller{
	protected function select(){
		$viewmodel = new LanguageModel();
		if(empty($_SERVER['HTTP_REFERER'])){
			header("location:".ROOT_URL);
		}else{
			header("location:".$_SERVER['HTTP_REFERER']);
		}
		return $viewmodel->select();
	}
}
