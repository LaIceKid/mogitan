<?php

class User extends Controller {
	protected function login() {
		$viewmodel = new UserModel;
		$this->ReturnView($viewmodel->login(), false);
	}

	protected function logout() {
		$viewmodel = new UserModel;
		$viewmodel->logout();
	}

	protected function checkLogin() {
		$viewmodel = new UserModel;
		$viewmodel->checkLogin();
	}

	protected function register() {
		$viewmodel = new UserModel;
		$this->ReturnView($viewmodel->register(), false);
	}

	protected function checkRegister() {
		$viewmodel = new UserModel;
		$viewmodel->checkRegister();
	}

	protected function items() {
		$viewmodel = new UserModel;
		$this->ReturnView($viewmodel->items(), true);
	}
	protected function add() {
		$viewmodel = new UserModel;
		$this->ReturnView($viewmodel->add(), true);
	}

}
