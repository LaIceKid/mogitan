<?php
class Home extends Controller{
	protected function index(){
		$viewmodel = new HomeModel();
		$this->ReturnView($viewmodel->index(), true);
	}
	protected function timeline(){
		$viewmodel = new HomeModel();
		$this->ReturnView($viewmodel->timeline(), false);
	}
}
