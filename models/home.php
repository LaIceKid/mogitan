<?php
class HomeModel extends Model{
	public function index(){
		return;
	}

	public function category($parent = 0){
		$this->query('SELECT * FROM category WHERE parentID = :parent');
		$this->bind('parent', $parent);
		$rows = $this->resultSet();
		return $rows;
	}

	public function last_item(){
		$this->query('SELECT id FROM product ORDER BY id DESC LIMIT 1');
		$row = $this->single();
		return $row['id'];
	}

	public function timeline(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		if(isset($get['offset']) && isset($get['limit'])){
			return $this->all_products($get['offset'], $get['limit']);
		}
		return $this->all_products();
	}

	public function all_products($offset = NULL, $limit = NULL){
		$infinite = "";
		if(!is_null($offset) && !is_null($limit)){
			$infinite = "WHERE p.id > ".$offset." ORDER BY p.id ASC LIMIT ".$limit;
		}
		$this->query('
			SELECT *, p.id as pid, c.id as cid FROM product p
			LEFT JOIN currency c
			ON p.product_currency = c.id
			LEFT JOIN product_status ps
			ON p.product_statusID = ps.id
			'.$infinite.'
		');
		$rows = $this->resultSet();
		//////////////////////////////////
		//Add relevant IMAGES to products
		foreach($rows as $pi_key => $product_image){
			foreach($this->product_images($product_image['pid']) as $image){
				$rows[$pi_key]['images'][] = $image['image_name'];
			}
		}
		//Add relevant IMAGES to products
		//////////////////////////////////

		//////////////////////////////////
		//Add relevant DISCOUNTS to products
		foreach($rows as $pd_key => $product_discount){
			foreach($this->product_discounts($product_discount['pid']) as $discount){
				$rows[$pd_key]['discounts'][] = $discount['product_discount_percent'];
			}
			//Get DISCOUNTED price if there is some
			if(!empty($rows[$pd_key]['discounts'])){
				$rows[$pd_key]['discounted_price'] = round($product_discount['product_price'] - ($product_discount['product_price'] * array_sum($rows[$pd_key]['discounts']) / 100));
				$rows[$pd_key]['discount_color'] = $this->discount_color(array_sum($rows[$pd_key]['discounts']));
			}
		}
		//Add relevant DISCOUNTS to products
		//////////////////////////////////
		return $rows;
	}

	public function product_images($productID = 0){
		$this->query('SELECT image_name FROM product_image WHERE productID = :productID');
		$this->bind('productID', $productID);
		$rows = $this->resultSet();
		return $rows;
	}

	public function product_discounts($productID = 0){
		$this->query('SELECT product_discount_percent FROM product_discount WHERE productID = :productID');
		$this->bind('productID', $productID);
		$rows = $this->resultSet();
		return $rows;
	}

	public function slider(){
		$this->query('SELECT * FROM slideshow ORDER BY sort ASC');
		$rows = $this->resultSet();
		return $rows;
	}

	public function discount_color($sum = 0){
		$color = '';
		if($sum >= 0 && $sum <= 10){
			$color = 'danger';
		}
		if($sum >= 11 && $sum <= 20){
			$color = 'warning';
		}
		if($sum >= 21){
			$color = 'success';
		}
		return $color;
	}

}
