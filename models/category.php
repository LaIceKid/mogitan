<?php
class CategoryModel extends Model{
  public function select(){
    $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
    $HomeModel = new HomeModel;

    if($this->check_sub_category($get['subroute']) === 0){
      $categorie_ids = $this->categorie_ids($get['route']);
      if(empty($categorie_ids)){
        $WHERE = "WHERE c.category_name = '".strtoupper(str_replace('-', '_', $get['route']))."'";
      }else{
        $WHERE = "WHERE cp.categoryID IN (".implode(',', $categorie_ids).")";
      }
    }else{
      $WHERE = "WHERE c.category_name = '".strtoupper(str_replace('-', '_', $get['subroute']))."'";
    }
    
    $this->query("
      SELECT *, p.id as pid, curr.id as currid FROM product p
      LEFT JOIN currency curr
      ON p.product_currency = curr.id
      LEFT JOIN product_status ps
      ON p.product_statusID = ps.id
      LEFT JOIN category_product cp
      ON p.id = cp.productID
      LEFT JOIN category c
      ON cp.categoryID = c.id
      ".$WHERE."
    ");
    $rows = $this->resultSet();
    //////////////////////////////////
    //Add relevant IMAGES to products
    foreach($rows as $pi_key => $product_image){
      foreach($HomeModel->product_images($product_image['pid']) as $image){
        $rows[$pi_key]['images'][] = $image['image_name'];
      }
    }
    //Add relevant IMAGES to products
    //////////////////////////////////

    //////////////////////////////////
    //Add relevant DISCOUNTS to products
    foreach($rows as $pd_key => $product_discount){
      foreach($HomeModel->product_discounts($product_discount['pid']) as $discount){
        $rows[$pd_key]['discounts'][] = $discount['product_discount_percent'];
      }
      //Get DISCOUNTED price if there is some
      if(!empty($rows[$pd_key]['discounts'])){
        $rows[$pd_key]['discounted_price'] = round($product_discount['product_price'] - ($product_discount['product_price'] * array_sum($rows[$pd_key]['discounts']) / 100));
      }
    }
    //Add relevant DISCOUNTS to products
    //////////////////////////////////

    return $rows;
  }

  public function check_sub_category($subroute = ''){
    $this->query("
      SELECT *, p.id as pid, curr.id as currid FROM product p
      LEFT JOIN currency curr
      ON p.product_currency = curr.id
      LEFT JOIN product_status ps
      ON p.product_statusID = ps.id
      LEFT JOIN category_product cp
      ON p.id = cp.productID
      LEFT JOIN category c
      ON cp.categoryID = c.id
      WHERE c.category_name = '".strtoupper(str_replace('-', '_', $subroute))."'
    ");
    $rows = $this->resultSet();
    if(!empty($subroute)){
      return 1;
    }
    if(count($rows) != 0){
      return 1;
    }
    return 0;
  }

  public function categorie_ids($route = ''){
    $this->query("
      SELECT id FROM category
      WHERE category_name = '".strtoupper(str_replace('-', '_', $route))."'
    ");
    $row = $this->single();

    $this->query("
      SELECT id FROM category
      WHERE parentID = :parentID
    ");
    $this->bind('parentID', $row['id']);
    $rows = $this->resultSet();
    $ids = array();
    foreach($rows as $ro){
      $ids[] = $ro['id'];
    }
    return $ids;
  }

}
