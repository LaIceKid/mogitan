<?php
//FIRST LOAD OF CURRENCY IS COMING FROM CLASSES/BOOTSTRAP.PHP
class CurrencyModel extends Model{
	public function select(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

		if(isset($get['id'])){
			$_SESSION['currency'] = $get['id'];
		}elseif(isset($_SESSION['currency'])){

		}else{
			$_SESSION['currency'] = 'EUR';
		}

		$this->query('SELECT * FROM currency WHERE currency_name = :currency_name');
		$this->bind(':currency_name', $_SESSION['currency']);
		$currencyRows = $this->resultSet();
		$currency = '';
		foreach($currencyRows as $curr){
			$currency = $curr['currency_name'];
		}
		if(isset($_GET['id'])){
			if(isset($_SERVER['HTTP_REFERER']) && parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == "mogitan.ge"){
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
		}
		$_SESSION['currency'] = $currency;
	}

	public function allCurrencies(){
		$this->query('SELECT * FROM currency');
		$currency = $this->resultSet();
		return $currency;
	}

	public function dispCurrency($name = ''){
		$this->query('SELECT * FROM currency WHERE currency_name = :currency_name');
		$this->bind(':currency_name', $name);
		$currency = $this->single();
		return $currency['currency_symbol'];
	}

	public function convert($convertible = 0, $curr = ''){
		$currency_data = $this->select_currency_latest_daily_rates($curr);
		if(empty($currency_data['currency_rate'])){
			return $convertible;
		}
		return round($convertible / $currency_data['currency_rate']);
	}

	public function select_currency_latest_daily_rates($curr = ''){
		$this->query('
      SELECT * FROM currency_daily_rates cdr
      RIGHT JOIN currency c
      ON cdr.currencyID = c.id
    ');
		$this->bind('currency_name', $curr);
		$rows = $this->resultSet();

		foreach ($rows as $key => $value) {
			if($value['currency_name'] == $curr){
				return $value;
			}
		}
	}

	public function select_currency_daily_rates($name = ''){
		$this->query('
      SELECT *, c.id as cid FROM currency c
      LEFT JOIN currency_daily_rates cdr
      ON c.id = cdr.currencyID
      WHERE c.currency_name = :currency_name ORDER BY c.id DESC
    ');
		$this->bind('currency_name', $name);
		$row = $this->single();
		return $row;
	}

	//download currency and insert into database
	public function download(){
		$curr = array();
		try{
			$client = new SoapClient('http://nbg.gov.ge/currency.wsdl');
			$curr = array(
				"EUR" => $client->GetCurrency('EUR'),
				"USD" => $client->GetCurrency('USD')
			);
    }catch (\SoapFault $e){
      $curr = array(
				"error" => "fatal"
			);
    }
		if(isset($curr['error'])){
			return;
		}

		foreach($curr as $key => $item){
      $daily_rates = $this->select_currency_daily_rates($key, date('Y-m-d'));
			echo '<pre>';
			print_r($daily_rates);
			echo '</pre>';
			if(
				($daily_rates['currency_rate'] != $item)
				&&
				(date(("Y-m-d"), strtotime($daily_rates['add_date'])) != date("Y-m-d"))
			){
				$this->query('INSERT INTO currency_daily_rates (currencyID, currency_rate, add_date) VALUES (:currencyID, :currency_rate, :add_date)');
				$this->bind('currencyID', $daily_rates['cid']);
				$this->bind('currency_rate', $item);
				$this->bind('add_date', date('Y-m-d H:i:s'));
				$this->execute();
				if($this->lastInsertId()){
					echo 'Inserted';
				}
			}
		}

		return $curr;
	}
}
