<?php
class LanguageModel extends Model{
	public function select(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

		$this->query('SELECT * FROM language WHERE determinant = :determinant');
		$this->bind(':determinant', $get['id']);
		$row = $this->single();

    $lang = 'en';
		if(!empty($row)){
			$lang = $row['determinant'];
		}

		$_SESSION['lang'] = $lang;
		return;
	}

	public function EngComparsion(){
		$this->query('SELECT * FROM translation WHERE langID = "1"');
		$rows = $this->resultSet();

		foreach($rows as $trans){
			$lang[$trans['shorten']] = $trans['translation'];
		}
		return $lang;
	}

	public function SelectLanguage(){
		$this->query('
			SELECT a.*, b.*
			FROM translation a
			INNER JOIN language b
			ON a.langID = b.id
			WHERE b.determinant = :lang
		');

		$language = 'en';
		if(isset($_SESSION['lang'])){
			$language = $_SESSION['lang'];
		}

		$this->bind(':lang', $language);
		$rows = $this->resultSet();

		if(empty($rows)){
			$langs = $this->EngComparsion();
		}else{
			foreach($rows as $trans){
				$lang[$trans['shorten']] = $trans['translation'];
			}
			$langs = array_merge($this->EngComparsion(), $lang);
		}
		return $langs;
	}

	public function allLangs(){
		$this->query('SELECT * FROM language');
		$langs = $this->resultSet();
		return $langs;
	}
}
