<?php
class UserModel extends Model{
  public function access(){
    $Other = new Other;
    $Other->access();
  }
  public function login(){
    $this->access();
  }

  public function checkLogin(){
    if(!isset($_POST)){
      return;
    }
    $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $this->query("SELECT * FROM user WHERE email = :email");
    $this->bind("email", $_POST['email']);
    $row = $this->single();
    if($row){
      if($row['password'] == password_verify($post['password'], $row['password'])){
        $this->create_user_data($row['id'], $row['email'], $row['first_name'], $row['last_name'], $row['avatar']);
        Messages::setMsg("LOGIN_SUCCESS", "success");
      }else{
        Messages::setMsg("LOGIN_INCORRECT_PASSWORD", "error");
      }
    }else{
      Messages::setMsg("LOGIN_USER_DOES_NOT_EXISTS", "error");
    }

    header("location:".ROOT_URL);
  }

  public function logout(){
    if(isset($_SESSION['user_data'])){
      unset($_SESSION['user_data']);
      Messages::setMsg("LOGOUT_SUCCESS", "success");
      header("location:".ROOT_URL);
    }
  }

  public function create_user_data($id = 0, $email = '', $name = '', $surname = '', $avatar = ''){
    $_SESSION['user_data']['is_logged_in'] = 1;
    $_SESSION['user_data']['id'] = $id;
    $_SESSION['user_data']['email'] = $email;
    $_SESSION['user_data']['first-name'] = $name;
    $_SESSION['user_data']['last-name'] = $surname;
    $_SESSION['user_data']['avatar'] = $avatar;
  }

  public function register(){
    // password_hash($password, PASSWORD_ARGON2I);
  }

  public function checkRegister(){
    if(!isset($_POST)){
      return;
    }
    $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $this->query("SELECT * FROM user WHERE email = :email");
    $this->bind("email", $post['email']);
    $row = $this->single();

    if($row){
      Messages::setMsg("REGISTER_USER_ALREADY_EXISTS", "error");
      header("location:".ROOT_URL);
      return;
    }
    if($post['password'] != $post['re-password']){
      Messages::setMsg("PASSWORDS_DOESNT_MATCH", "error");
      header("location:".ROOT_URL);
      return;
    }
    foreach($post as $item){
      if(empty($item)){
        Messages::setMsg("FILL_IN_ALL_FIELDS", "error");
        header("location:".ROOT_URL);
        return;
      }
    }

    echo '<pre>';
    print_r($_POST);
    print_r($_FILES);
    echo '</pre>';

    $Other = new Other;

    $avatar = $Other->image_upload($_FILES['file'], 'assets/images/avatar/');

    //INSERT INTO DATABASE AND REDIRECT
    if($this->insert_user($post, $avatar)){
      Messages::setMsg("REGISTER_SUCCESS", "success");
      header("location:".ROOT_URL);
    }else{
      Messages::setMsg("REGISTER_SOMETHING_WENT_WRONG", "error");
      header("location:".ROOT_URL);
    }
  }

  public function insert_user($post = '', $avatar = NULL){
    $keys = array();
    $keyVals = array();
    unset($post['re-password']);
    $post['password'] = password_hash($post['password'], PASSWORD_ARGON2I);
    if(!empty($avatar)){
      $post['avatar'] = $avatar;
    }
    foreach($post as $key => $value){
      $keys[] = str_replace('-', '_', $key);
      $vals[] = "'".$value."'";
    }
    $impKeys = implode(', ', $keys);
    $impVals = implode(', ', $vals);

    $this->query("INSERT INTO user ($impKeys) VALUES ($impVals)");
    $this->execute();
    return $this->lastInsertId();
  }

  public function items(){
  	
  }

  public function add(){

  }
}
