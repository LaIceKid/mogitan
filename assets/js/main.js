$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
});


$('.carousel').carousel();

//ONLOAD FUNTIONS
// ajaxProcess($("#login-container"),"login-container");


function ajaxProcess(element, container) {
    let cont = $("#" + container);
    cont.html('');
    cont.append('<div id="preloader-' + container + '" class="lds-ring"><div></div><div></div><div></div><div></div><div></div></div>');

    let url = $(element).data('href');

    let request = $.post(url, {key: "success"}, function () {

    });

    request.done(function (response) {
        cont.html(response);
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}

// function check_login(event){
//   var form = $("#login-form");
//
//   $(form).submit(function(e){
//     e.preventDefault();
//     $.post('user/checklogin/', $(this).serialize(),
// 		function(data) {
// 			var response = $(data);
// 			//var container = response.find('#containerID').html();
// 			$("#login-response").html(response);
// 			$("#login-response").fadeIn(300);
// 			$("#preloader").fadeOut(100);
//       $(".message-display").hide();
// 	}).always(function() {
//     $(".message-display").fadeIn(300);
//     message_fade_out();
// 	});
//
//   });
// }

let message_fade_out = () => {
    if ($(".message-display").length > 0) {
        setTimeout(function () {
            $(".message-display").fadeOut(300);//FADE OUT MESSAGE

            setTimeout(function () {
                $(".message-display").remove();//REMOVE MESSAGE
            }, 500);

        }, 3000);
    }
};
message_fade_out();


////////////////////////////////////////////////////////
/////////////////[ INFINITE SCROLL ]////////////////////
$(document).ready(function () {
    getMoreData();
    windowOnScroll();
});

let windowOnScroll = () => {
    $(window).on("scroll", function (e) {
        let timeline = $("#timeline");
        if (Math.ceil($(window).scrollTop()) === $(document).height() - $(window).height()) {
            $("#no-more-items").remove();
            let last_item = timeline.find(".product").last();
            let offset = last_item.data("id");
            if (last_item.data("last-item-id") !== offset) {
                getMoreData(offset);
            } else {
                timeline.append('<div id="no-more-items">THIERE ARE NO MORE ITEMS IN THE DATABASE</div>');
            }
        }
    });
};

let getMoreData = (offset = 1) => {
    $(window).off("scroll");
    $.ajax({
        url: 'http://mogitan.loc/timeline/?offset=' + offset + '&limit=2',
        type: "get",
        beforeSend: function () {
            $("#timeline").append($('.ajax-loader').show());
        },
        success: function (data) {
            setTimeout(function () {
                $('.ajax-loader').hide();
                $("#timeline").append(data);
                windowOnScroll();
            }, 1000);
        }
    });
};
/////////////////[ INFINITE SCROLL ]////////////////////
////////////////////////////////////////////////////////
