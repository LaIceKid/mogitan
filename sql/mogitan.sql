-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2019 at 01:41 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mogitan`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `category_name` varchar(150) NOT NULL,
  `icon` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parentID`, `category_name`, `icon`) VALUES
(1, 0, 'FURNITURE_INTERIOR', 'furniture.png'),
(2, 0, 'COLLECTING', 'collecting.png'),
(3, 0, 'AUDIO_VIDEO', 'audio-video.png'),
(4, 0, 'FOR_KIDS', 'kids.png'),
(5, 0, 'FISHING_HUNTING_WEAPON', 'fishing-hunting-weapon.png'),
(6, 0, 'COMPUTER_PARTS', 'computer-parts.png'),
(7, 0, 'MOBILE_PHONE', 'phone.png'),
(8, 0, 'MUSICAL_INSTRUMENTS', 'musical-instruments.png'),
(9, 0, 'AGRICULTURE', 'agriculture.png'),
(10, 0, 'JEWELRY', 'jewelry.png'),
(11, 0, 'CHANCELLERY', 'chancellery.png'),
(12, 0, 'HOME_APPLIANCES', 'home-appliances.png'),
(13, 0, 'HEALTH_BEAUTY', 'health-beauty.png'),
(14, 0, 'HANDMADE', 'handmade.png'),
(15, 0, 'CLOTHES_ACCESSORIES', 'clothes-accessories.png'),
(16, 0, 'ANIMALS_PLANTS', 'animals-plants.png'),
(17, 0, 'BOOKS_MUSIC_VIDEO', 'books-music-video.png'),
(19, 1, 'FIRST_TEST_CATEGORY', NULL),
(20, 1, 'SECOND_TEST_CATEGORY', NULL),
(21, 1, 'THIRD_TEST_CATEGORY', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE `category_product` (
  `id` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `productID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`id`, `categoryID`, `productID`) VALUES
(7, 21, 7),
(8, 19, 8),
(9, 19, 9),
(10, 19, 10),
(11, 19, 11),
(12, 19, 11),
(13, 19, 13),
(14, 19, 13),
(15, 19, 12),
(16, 19, 14),
(17, 19, 13),
(18, 20, 15),
(19, 20, 16);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `currency_name` varchar(3) NOT NULL,
  `currency_code` int(11) NOT NULL,
  `currency_symbol` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency_name`, `currency_code`, `currency_symbol`) VALUES
(1, 'GEL', 981, '₾'),
(2, 'USD', 840, '$'),
(3, 'EUR', 978, '€');

-- --------------------------------------------------------

--
-- Table structure for table `currency_daily_rates`
--

CREATE TABLE `currency_daily_rates` (
  `id` int(11) NOT NULL,
  `currencyID` int(11) NOT NULL,
  `currency_rate` float NOT NULL,
  `add_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency_daily_rates`
--

INSERT INTO `currency_daily_rates` (`id`, `currencyID`, `currency_rate`, `add_date`) VALUES
(5, 3, 3.0327, '2019-03-23 23:25:48'),
(6, 2, 2.6821, '2019-03-23 23:25:48'),
(7, 3, 3.0349, '2019-03-25 18:44:13'),
(8, 2, 2.6822, '2019-03-25 18:44:13'),
(9, 3, 3.0203, '2019-03-31 15:46:34'),
(10, 2, 2.6914, '2019-03-31 15:46:34');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `determinant` varchar(2) NOT NULL,
  `culture_name` varchar(100) NOT NULL,
  `img` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `display_name`, `determinant`, `culture_name`, `img`) VALUES
(1, 'ქართული', 'ge', 'ka-GE', 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/Flag_of_Georgia.svg/255px-Flag_of_Georgia.svg.png'),
(2, 'English', 'en', 'en-EN', 'https://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Flag_of_the_United_States.svg/300px-Flag_of_the_United_States.svg.png'),
(3, 'Русский', 'ru', 'ru-RU', 'https://upload.wikimedia.org/wikipedia/en/thumb/f/f3/Flag_of_Russia.svg/255px-Flag_of_Russia.svg.png');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_price` float DEFAULT NULL,
  `product_currency` int(11) DEFAULT NULL,
  `product_statusID` int(11) DEFAULT NULL,
  `product_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `product_price`, `product_currency`, `product_statusID`, `product_date`) VALUES
(1, 'PLACEHOLDER', 50, 1, 1, '2019-06-02 22:27:19'),
(7, 'წიგნი - Emergency care', 491, 1, 1, '2019-06-02 12:53:49'),
(8, 'მანქანის ნიტროს კონტეინერი - NOS', 273, 1, 1, '2019-06-02 12:56:19'),
(9, 'Custom Spider - ველოსიფედი', 810, 1, 1, '2019-06-03 12:34:43'),
(10, '4-Wheel Scooter - 4 ბორბლიანი სკუტერი', 3780, 1, 1, '2019-06-03 12:34:43'),
(11, 'Yacht Margregor 26M - იახტა მარგრეგორ 26მ', 51297, 1, 1, '2019-06-03 12:34:43'),
(12, ' 1910-1920 Vintage Child\'s Pedal Airplane- საბავშვო თვითმფრინავი', 1215, 1, 1, '2019-06-03 12:34:43'),
(13, 'Swing set - საბავშვო სასრიალო', 122, 1, 1, '2019-06-03 14:26:29'),
(14, 'Oil Painting Supplies - ზეთის საღებავები', 405, 1, 1, '2019-06-03 12:34:43'),
(15, 'Chicken Coop -  საქათმე', 2200, 1, 1, '2019-06-03 12:34:43'),
(16, 'Motorcycle helmet - ჩაფხუტი', 189, 1, 1, '2019-06-03 12:34:43');

-- --------------------------------------------------------

--
-- Table structure for table `product_discount`
--

CREATE TABLE `product_discount` (
  `id` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `product_discount_percent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_discount`
--

INSERT INTO `product_discount` (`id`, `productID`, `product_discount_percent`) VALUES
(3, 7, 20),
(4, 8, 10);

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE `product_image` (
  `id` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `image_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`id`, `productID`, `image_name`) VALUES
(1, 1, 'a0d63c2001e211945d6842545e054fae.jpg'),
(2, 1, 'f460da6e651ebc2ec9c7459f81c1fbff.jpg'),
(3, 2, '64c0a84b3c85ea39916dca05cd5d61c8.jpg'),
(4, 3, '103aea8cfb5c5dedea7542f81d765b11.jpg'),
(5, 4, 'f39f660ee7a8301d68097a8f5a6359d4.jpg'),
(6, 5, '40acfdf6bd0db5ec3bd6135d7a11fd0a.jpg'),
(7, 6, '946a2d3212b84b5d874000c3596fab67.jpg'),
(8, 7, '4482e862863b66219141f8b2596603af.jpg'),
(9, 8, '6aff4da977b78f8a1307c30ee22e1382.jpg'),
(10, 8, '1b10de2e6cfb75548903099782815bdd.jpg'),
(11, 8, '5338a5b7ccf3f1f3c05adef07f01fd05.jpg'),
(12, 9, '7629d3c83394264c4e82bebd67e9a509.jpg'),
(13, 9, 'b44beeccbea4557a19bf184524131aad.jpg'),
(14, 9, 'f1e799ab1cba05781bcf6fe2c5f54add.jpg'),
(15, 10, '21f7404fe0c35829982760cb6313b009.jpg'),
(16, 10, '5eb40367caa70e598e8217543834a95b.jpg'),
(17, 10, '9900a711911f2c3bfc027cc5b66354e2.jpg'),
(18, 10, 'f038eb363e9e4769da1b70dcf5b8a1ad.jpg'),
(19, 11, '749c8e3e110566ebfc8d2b0ce33e5b31.jpg'),
(20, 11, 'f6b67291ad12c2ef46996c54b0dff192.jpg'),
(21, 11, 'beef61eddb493a8523190c973e7cc982.jpg'),
(22, 11, 'f25ff32720107b77824ed9c322f1f802.jpg'),
(23, 11, '9a5aec69f66568452fc7cfee4eec0119.jpg'),
(24, 12, '4a0c5618057704a2f244f82d6d1531a7.jpg'),
(25, 12, '4a2eaae5fb9ac39a685f9643b3235760.jpg'),
(26, 12, '1809161f8d7755e33e640045a8961b8d.jpg'),
(27, 12, 'af2b4ae1b3aa0ea9cd3b930ce6dcaacb.jpg'),
(28, 14, 'be40848f75f7b1f79ccc873b6c9b3a37.jpg'),
(29, 12, 'c425a27b15d7613b54eb568098c3df8e.jpg'),
(30, 12, 'dae708d76ac2bf2fdb8f4e151433fbee.jpg'),
(31, 12, '733768a4e05d27425590cd3b4739ed1a.jpg'),
(32, 12, 'a6d84b880e9d8a8cab316ded04fbec5a.jpg'),
(33, 13, '3a69c6e0a6a06a94388891edda508e82.jpg'),
(34, 13, '9125f56d3932503886fe3273295ea6ba.jpg'),
(35, 13, '69700070eae490818065c661ac358d4c.jpg'),
(36, 15, '9d8e1ffdcdb7368766226e8315a26c8b.jpg'),
(37, 15, '01292980c7653b46c0eb4e93daa339b1.jpg'),
(38, 15, 'ffcb499904db1e1a1092f4b9d4fce391.jpg'),
(39, 16, '2af76d60f61879465714277fce9aab50.jpg'),
(40, 16, '75c255d149acbfb231cafea9eb78aad4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_status`
--

CREATE TABLE `product_status` (
  `id` int(11) NOT NULL,
  `product_status` varchar(100) NOT NULL,
  `status_color` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_status`
--

INSERT INTO `product_status` (`id`, `product_status`, `status_color`) VALUES
(1, 'IN_STOCK', 'text-green'),
(2, 'OUT_OF_STOCK', 'text-red'),
(3, 'DELIVERY_IN_12_DAYS', 'text-light-gray');

-- --------------------------------------------------------

--
-- Table structure for table `slideshow`
--

CREATE TABLE `slideshow` (
  `id` int(11) NOT NULL,
  `slider_title` varchar(255) NOT NULL,
  `slider_description` text NOT NULL,
  `slider_image` varchar(100) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slideshow`
--

INSERT INTO `slideshow` (`id`, `slider_title`, `slider_description`, `slider_image`, `sort`) VALUES
(1, 'The House Targaryen Stratocaster', 'Fender says that they “worked directly and extensively with ‘Game of Thrones’ creator and avid guitar player D.B. Weiss to ensure that our vision, design and materials reflected the identity of each house,” which helps explain, at least in part, why these designs are so sweet.', 'c4ca4238a0b923820dcc509a6f75849b.jpg', 1),
(2, 'AMD R5 1600X PC', 'It’s been a few months since our last PC build--in fact, it was published well before Ryzen was released. For our first post-Ryzen build, we’ve pulled together some of the components we liked best in testing to make an affordable ultrawide gaming machine.', 'c81e728d9d4c2f636f067f89cc14862c.jpg', 2),
(3, 'T700 carbon fibre reinforced composite', 'We design, we engineer high-end, smart and connected e-Bikes with amazing technology and performance. All of us are fascinated about technology and we are not afraid to use technology to upgrade your riding experience. This is our mission.', 'eccbc87e4b5ce2fe28308fd9f2a7baf3.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `translation`
--

CREATE TABLE `translation` (
  `id` int(11) NOT NULL,
  `langID` int(11) NOT NULL,
  `shorten` varchar(255) NOT NULL,
  `translation` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `translation`
--

INSERT INTO `translation` (`id`, `langID`, `shorten`, `translation`, `date`) VALUES
(1, 1, 'HOME', 'მთავარი', '2019-03-05 00:00:00'),
(2, 2, 'HOME', 'Home', '2019-03-05 00:00:00'),
(3, 3, 'HOME', 'Главная Страница', '2019-03-05 00:00:00'),
(4, 1, 'ALL_FOR_GROWING', 'ყველაფერი ზრდისთვის', '2019-03-05 00:00:00'),
(5, 1, 'ALL_FOR_SMOKERS', 'ყველაფერი მწეველებისთვის', '2019-03-05 00:00:00'),
(26, 1, 'IN_STOCK', 'საწყობშია', '2019-03-22 00:00:00'),
(27, 1, 'OUT_OF_STOCK', 'არ არის საწყობში', '2019-03-22 00:00:00'),
(28, 1, 'DELIVERY_IN_12_DAYS', 'მიწოდება 12 დღეში', '2019-03-22 00:00:00'),
(29, 1, 'LOGIN_SUCCESS', 'თქვენ წამრატებით გაიარეთ ავტორიზაცია', '2019-04-04 00:00:00'),
(30, 1, 'LOGIN_USER_DOES_NOT_EXISTS', 'ასეთი მომხმარებელი არ არის რეგისტრირებული...', '2019-04-04 00:00:00'),
(31, 1, 'LOGIN_INCORRECT_PASSWORD', 'პაროლი არასწორია...', '2019-04-04 00:00:00'),
(32, 1, 'LOGOUT_SUCCESS', 'თქვენ წამრატებით გამოხვედით სისტემიდან...', '2019-04-04 00:00:00'),
(33, 1, 'REGISTER_USER_ALREADY_EXISTS', 'მომხმარებელი ასეთი ელ. ფოსტით უკვე რეგისტრირებულია...', '2019-04-05 00:00:00'),
(34, 1, 'REGISTER_SUCCESS', 'რეგისტრაცია წარმატებით დასრულდა. შეგიძლიათ შეხვიდეთ სისტემაში...', '2019-04-05 00:00:00'),
(35, 1, 'PASSWORDS_DOESNT_MATCH', 'პაროლები არ ემთხვევა...', '2019-04-05 00:00:00'),
(36, 1, 'FILL_IN_ALL_FIELDS', 'შეავსეთ ყველა ველი...', '2019-04-05 00:00:00'),
(37, 1, 'REGISTER_SOMETHING_WENT_WRONG', 'მოხდა ამოუცნობი შეცდომა რეგისტრაციის დროს... გთხოვთ თავიდან ცადეთ ან დაუკავშირდით ადმინისტრაციას...', '2019-04-05 00:00:00'),
(38, 1, 'LOGIN', 'ავტორიზაცია', '2019-04-08 00:00:00'),
(39, 1, 'EMPTY_CATEGORY', 'კატეგორია ცარიელია...', '2019-04-08 00:00:00'),
(40, 1, 'YOU_DONT_HAVE_A_PERMISSION_TO_SEE_THIS_PAGE', 'თქვენ არ გაქვთ ამ გვერდის ნახვის უფლება...', '2019-04-08 00:00:00'),
(41, 1, 'BOOKS_MUSIC_VIDEO', 'წიგნები, მუსიკა, ვიდეო', '2019-04-08 00:00:00'),
(43, 1, 'ANIMALS_PLANTS', 'ცხოველები, მცენარეები', '2019-04-08 00:00:00'),
(44, 1, 'CLOTHES_ACCESSORIES', 'ტანსაცმელი, აქსესუარები', '2019-04-08 00:00:00'),
(45, 1, 'HANDMADE', 'ხელნაკეთი', '2019-04-08 00:00:00'),
(46, 1, 'HEALTH_BEAUTY', 'ჯანმრთელობა, სილამაზე', '2019-04-08 00:00:00'),
(47, 1, 'HOME_APPLIANCES', 'საოჯახო ტექნიკა', '2019-04-08 00:00:00'),
(48, 1, 'CHANCELLERY', 'საკანცელარიო ნივთები', '2019-04-08 00:00:00'),
(49, 1, 'JEWELRY', 'საიუველირო ნაწარმი', '2019-04-08 00:00:00'),
(50, 1, 'AGRICULTURE', 'სოფლის მეურნეობა', '2019-04-08 00:00:00'),
(51, 1, 'MUSICAL_INSTRUMENTS', 'მუსიკალური ინსტრუმენტები', '2019-04-08 00:00:00'),
(52, 1, 'MOBILE_PHONE', 'მობილურები, ტელეფონები', '2019-04-08 00:00:00'),
(53, 1, 'COMPUTER_PARTS', 'კომპიუტერები (ნაწილები)', '2019-04-08 00:00:00'),
(54, 1, 'FISHING_HUNTING_WEAPON', 'იარაღი, თევზაობა, ნადირობა', '2019-04-08 00:00:00'),
(55, 1, 'FOR_KIDS', 'ბავშვთა სამყარო', '2019-04-08 00:00:00'),
(56, 1, 'AUDIO_VIDEO', 'აუდიო/ვიდეო', '2019-04-08 00:00:00'),
(57, 1, 'COLLECTING', 'კოლექციონერობა', '2019-04-08 00:00:00'),
(58, 1, 'FURNITURE_INTERIOR', 'ავეჯი, ინტერიერი', '2019-04-08 00:00:00'),
(59, 1, 'SOFA', 'სავარძელი', '2019-04-08 00:00:00'),
(60, 1, 'FIRST_TEST_CATEGORY', 'პირველი სატესტო კატეგორია', '2019-04-08 00:00:00'),
(61, 1, 'SECOND_TEST_CATEGORY', 'მეორე სატესტო კატეგორია', '2019-04-08 00:00:00'),
(62, 1, 'THIRD_TEST_CATEGORY', 'მესამე სატესტო კატეგორია', '2019-04-08 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `avatar` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `mobile`, `avatar`) VALUES
(1, 'taduxe@mailinator.com', '$argon2i$v=19$m=1024,t=2,p=2$T3p4b0VBbk1rUDBENUl3Sw$8pa8weFm3EUWrWsuke0BlCJtUrR6icrvxo0gCJ461CQ', 'Barrett', 'Weber', '+1 (188) 546-9243', '52fbda7ad992cb509d202373345ca1a6.jpg'),
(3, 'lasha.wm@gmail.com', '$argon2i$v=19$m=1024,t=2,p=2$enl2U0pVNzFXejlSelpweQ$8ZyPVnWJtHLegUGiYXC2ZgyHuwsd/RviKWPTFpqdcIk', 'Lasha', 'Mamatelashvili', '593675929', '634eae25ec9fe11f37827170450a34eb.jpg'),
(4, 'jadowibom@mailinator.com', '$argon2i$v=19$m=1024,t=2,p=2$OVZBS3UybG4zUlUvM1k3cw$HKcsMYzcRLOpe4Qmr682xwk4vSGAhiFVZdlOFbqFyF0', 'Jenette', 'Stokes', '+1 (328) 163-2905', '94c9a6403eba30f457716dfbd47b1e59.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productID` (`productID`),
  ADD KEY `categoryID` (`categoryID`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_daily_rates`
--
ALTER TABLE `currency_daily_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_discount`
--
ALTER TABLE `product_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_status`
--
ALTER TABLE `product_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slideshow`
--
ALTER TABLE `slideshow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translation`
--
ALTER TABLE `translation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `langID` (`langID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `category_product`
--
ALTER TABLE `category_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `currency_daily_rates`
--
ALTER TABLE `currency_daily_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `product_discount`
--
ALTER TABLE `product_discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_image`
--
ALTER TABLE `product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `product_status`
--
ALTER TABLE `product_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `slideshow`
--
ALTER TABLE `slideshow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `translation`
--
ALTER TABLE `translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category_product`
--
ALTER TABLE `category_product`
  ADD CONSTRAINT `category_product_ibfk_1` FOREIGN KEY (`categoryID`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `category_product_ibfk_2` FOREIGN KEY (`productID`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `translation`
--
ALTER TABLE `translation`
  ADD CONSTRAINT `translation_ibfk_1` FOREIGN KEY (`langID`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
